import $ from 'jquery'

var Ajaxio = {
    root: '/',
    homeId: 'home',
    previousPageId: '',
    currentPageId: '',
    onFirstLoad: null,
    beforePageChangeCallbacks: [],
    beforePageChange: {
        add: function(callback) {
            Ajaxio.beforePageChangeCallbacks.push(callback);
        }
    },
    afterPageChangeCallbacks: [],
    afterPageChange: {
        add: function(callback) {
            Ajaxio.afterPageChangeCallbacks.push(callback);
        }
    },
    fileExtension: '.html',
    targetSelector: '',
    pageScripts: [],
    languagesUrl: [],
    currentLanguage: ''
};

Ajaxio.init = function(params) {
    // Définition des variables de configuration
    Ajaxio.root = typeof params.root == 'string' ? params.root : '/';
    Ajaxio.homeId = typeof params.homeId == 'string' ? params.homeId : Ajaxio.homeId;
    Ajaxio.fileExtension = typeof params.fileExtension == 'string' ? params.fileExtension : Ajaxio.fileExtension;
    Ajaxio.onFirstLoad = typeof params.onFirstLoad == 'function' ? params.onFirstLoad : null;
    Ajaxio.containerSelector = typeof params.containerSelector == 'string' ? params.containerSelector : document.body;
    Ajaxio.targetSelector = typeof params.targetSelector == 'string' ? params.targetSelector : Ajaxio.targetSelector;
    Ajaxio.languagesUrl = typeof params.languagesUrl == 'object' ? params.languagesUrl : Ajaxio.languagesUrl;

    // Récupération du langage et du nom de la page demandée dans l'url
    let urlInfos = Ajaxio._processUrl();
    Ajaxio.currentLanguage = urlInfos.language;
    let currentPageName = urlInfos.pageId;
    Ajaxio.currentPageId = currentPageName ? currentPageName : Ajaxio.homeId;     
    
    //Ajaxio.changePage(Ajaxio.currentPageId, true);
    if (typeof Ajaxio.pageScripts[Ajaxio.currentPageId] == 'function') {
        Ajaxio.pageScripts[Ajaxio.currentPageId]();
    }
    
    if (Ajaxio.onFirstLoad) Ajaxio.onFirstLoad(Ajaxio.currentPageId);

    window.addEventListener('popstate', function(){
        let pageId = Ajaxio._processUrl().pageId;
        Ajaxio._updatePageFromId(pageId); 
    });
}

/*
    Retrieves name of the page and language from the url
*/
Ajaxio._processUrl = function() {
    let pageId = window.location.pathname.replace(Ajaxio.root, '');
    let language = '';
    let splitUrl = pageId.split('/');

    // Prevents 'fr/' from being splitUrl to an array of length two (['fr', ''])
    if (splitUrl.length > 1 && splitUrl[splitUrl.length - 1] == "") splitUrl.pop(); 

    // One url item
    if (splitUrl.length == 1) {
        // Url item is a language
        if (Ajaxio.languagesUrl.includes(splitUrl[0])) {
            pageId = '';
            language = splitUrl[0] + '/';
            
            // Converts '/fr' to '/fr/'
            if (!window.location.pathname.endsWith('/')) window.history.replaceState({}, document.title, splitUrl[0] + '/');
        } 
        // Url item is a page
        else {
            pageId = splitUrl[0];
            language = '';
        }
    }
    // Several url items
    else if (splitUrl.length > 1) {
        // First url item is a language
        if (Ajaxio.languagesUrl.includes(splitUrl[0])) {
            pageId = splitUrl[1];
            language = splitUrl[0] + '/';
        } 
        else {
            pageId = splitUrl[0];
            language = '';
        }
    } 

    return {
        pageId: pageId == '' ? Ajaxio.homeId : pageId,
        language: language
    }
}

Ajaxio.changePage = function(pageId, force) {
    if (pageId == Ajaxio.currentPageId && !force) return;
         
    // First change URL
    Ajaxio._updateURL(pageId);
    // Then display page according to the URL    
    Ajaxio._updatePageFromId(pageId);    
}

Ajaxio._updateURL = function(pageName) {
    var pageUrl;
    if (Ajaxio.currentLanguage == '') {
        pageUrl = (pageName == Ajaxio.homeId) ? Ajaxio.root : Ajaxio.root + pageName;
    } else {
        pageUrl = (pageName == Ajaxio.homeId) ? Ajaxio.root + Ajaxio.currentLanguage : Ajaxio.root + Ajaxio.currentLanguage + pageName;
    }
    window.history.pushState({}, '', pageUrl);
}

Ajaxio._updatePageFromId = function(pageId) {
    if (Ajaxio.beforePageChangeCallbacks.length > 0) {
        Ajaxio.beforePageChangeCallbacks.forEach(callback => {
            callback(displayPage, Ajaxio.currentPageId, pageId);
        }); 
    } 
    else {
        displayPage();
    }
    
    function displayPage() {
        let target = Ajaxio.targetSelector == '' ? '' : ' ' + Ajaxio.targetSelector + ' > *';
        $(Ajaxio.containerSelector).load(pageId + Ajaxio.fileExtension + target , (resp, status) => {
            if (status == "success") {
                Ajaxio.previousPageId = Ajaxio.currentPageId;
                Ajaxio.currentPageId = pageId;
                
                if (Ajaxio.afterPageChangeCallbacks.length > 0) {
                    Ajaxio.afterPageChangeCallbacks.forEach(callback => {
                        callback(Ajaxio.currentPageId, Ajaxio.previousPageId);
                    });
                }

                if (typeof Ajaxio.pageScripts[Ajaxio.currentPageId] == 'function') {
                    Ajaxio.pageScripts[Ajaxio.currentPageId]();
                }
            } 
            else {
                console.error("[Ajaxio] Ajax load failed.")
            }
        });
    }
}

export default Ajaxio;
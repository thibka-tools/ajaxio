# Demo

https://thibka.github.io/ajaxio/public/

# Install

```bash
npm i ajaxio
```

# Table of Contents
1. [Webpack Configuration](#conf)
    1. [Create an .htaccess](#conf-htaccess)
    2. [Load each html page with html-webpack-plugin](#conf-HtmlWebpackPlugin)
2. [Usage](#usage)
    1. [Init](#usage-init)
    2. [Options](#usage-options)
    3. [Methods](#usage-methods)
    4. [Properties](#usage-properties)
3. [Limitation](#limitation)

# Webpack Configuration <a name="conf"></a>

## Create an .htaccess <a name="conf-htaccess"></a>
Create an .htaccess in your webpack source folder with the following content.
```apacheconf
<IfModule mod_rewrite.c>
RewriteEngine on
RewriteBase /
RewriteRule ^index\.html$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.html [L]
</IfModule>
```

If your files are in a subdirectory (for example https://mysite.com/myproject/), set your htaccess this way and don't forget to set the [*root* option](#usage-options) accordindly during initialisation.
```apacheconf
<IfModule mod_rewrite.c>
RewriteEngine on
RewriteBase /myproject
RewriteRule ^/myproject/index\.html$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /myproject/index.html [L]
</IfModule>
```

*Webpack tip*: to load the htaccess from your src folder to your production build, use copy-webpack-plugin in your webpack.config.js:
```javascript
const CopyWebpackPlugin = require('copy-webpack-plugin');
let config = {
    // ...
    plugins: [
        // ...
        new CopyWebpackPlugin({
			patterns: [
				{ from: 'src/.htaccess' },
			]
		})
    ]
}
```
*Snowpack tip*: to load the htaccess from your src folder to your production build, use snowpack-plugin-copy in your snowpack.config.js:
```javascript
module.exports = {
    // ...
    plugins: [
        // ...
        [
            'snowpack-plugin-copy',
            {
                patterns: [
                    {
                        source: 'src/.htaccess',
                        destination: 'build'
                    }
                ],
            },
        ]
    ]
}
```
Also, if you use Snowpack, set it up to accept html fragments.
```javascript
module.exports = {
    // ...
    buildOptions: {
        htmlFragments: true
    }
}
```

## Load each html page with html-webpack-plugin <a name="conf-HtmlWebpackPlugin"></a>
Ajaxio works by:
1. Receiving the name of the page the user wants to access (either from the method changePage, or from the url)
2. Retrieving html content from other pages (other html files)
3. Loading this content into index.html

So we need webpack to load each page from the source folder to the build.  
To do so, use html-webpack-plugin in your webpack.config.js:

```javascript
const HtmlWebpackPlugin = require('html-webpack-plugin');
let config = {
    plugins: [
        //...

        // First we load the main html template
        new HtmlWebpackPlugin({ 
            filename: './index.html',
            template: './src/index.html'
        }),
        // Then, every other asynchronously loaded contents
        new HtmlWebpackPlugin({
            filename: './page-content-1.html',
            template: './src/page-content-1.html',
            inject: false // <- this has to be set to false for any other page than index.html
        }),
        new HtmlWebpackPlugin({
            filename: './page-content-2.html',
            template: './src/page-content-2.html',
            inject: false // <- this has to be set to false for any other page than index.html
        })
    ]
}
```
Or you could also do it this way
```javascript
const HtmlWebpackPlugin = require('html-webpack-plugin');
let config = {
    plugins: [
        //...
        // First we load the main html template
        new HtmlWebpackPlugin({
			template: './src/index.html'
		}),
        // Then, every other asynchronously loaded contents
        ...(['page-content-1', 'page-content-2'].map((file) => new HtmlWebpackPlugin({
			filename: `./${file}.html`,
			template: `./src/${file}.html`,
			inject: false // <- this has to be set to false for any other page than index.html
		}))),
    ]
}
```

# Usage <a name="usage"></a>
## Init <a name="usage-init"></a>
```javascript
import Ajaxio from 'ajaxio'

Ajaxio.init();
```

## Options <a name="usage-options"></a>
The following options can be passed in the init() method.  
Example : 
```javascript
Ajaxio.init({
    root: '/root-folder/',
    // Specifies the appropriate subfolder path if the website is not at the root of the domain.
    // Default is '/'.

    containerSelector: '#load-content-here',
    // CSS selector of the element receiving the ajax loaded HTML content.
    // Default is 'body'.

    targetSelector: '#retreive-me',
    // CSS selector of the target element where Ajaxio should retrieve content in the external html files. Only the content of the selected element will be loaded.
    // Default is empty (which means the whole file content is retrieved).

    fileExtension: '.php',
    // Specifies what kind of files will be used to retrieve content.
    // Default is '.html'

    homeId: 'welcome',
    // Specifies the id of the home page to prevent displaying it in the url.
    // For instance when ajaxio loads "home.html" we don't want the url to be "domain.com/home".
    // Default is 'home'

    languagesUrl: ['fr', 'it'],
    // Optionnaly set languages when the url accepts languages code.
    // Exemple myurl.com/fr/, myurl.com/fr/projects

    onFirstLoad: currentPageId => { 
        console.log(currentPageId + ' has been loaded');
    }
    // Callback function triggered on first page load.
    // This function will only be triggered once, unless the browser page is refreshed,
    // and provides the current page id as parameter.
});
```

# Methods <a name="usage-methods"></a>
### changePage
Loads an HTML page from a given name.
```javascript
Ajaxio.changePage('pageId'); // Will load "./pageId.html", unless another file extension has been defined in the initialization options.
```

### beforePageChange
```javascript
Ajaxio.beforePageChange.add((done, previousPageId, nextPageId) => {            
    // Triggered before a new page is loaded.
    // The "done" parameter is a callback function that needs to be manually called once.
    // This will allow you to control when the page change will actually be triggered.
    done();
});
```

### afterPageChange
```javascript
Ajaxio.afterPageChange.add((nextPageId, previousPageId) => {
    // Triggered once the new page has been loaded.
});
```

## Properties <a name="usage-properties"></a>

### pageScripts
Ajaxio.pageScript is an array used to declare callback functions tied to pages.  
Each callback is triggered when the page sharing its name is loaded, right after the afterChange hook.

src/js/home.js
```javascript
import Ajaxio from 'ajaxio'

export default function home() {
    document.getElementById('link').addEventListener('click', ()=>{
        Ajaxio.changePage('page2');
    });
}
```
src/index.js
```javascript
import home from './js/home'

Ajaxio.pageScripts['home'] = home;
```

# Limitation of Ajaxio <a name="limitation"></a>

Even if the url changes, ajaxio uses the index.html page only.  
This means that each page will share the same \<head> tag.